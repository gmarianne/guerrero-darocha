import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
    },
    {
        path: '/books',
        name: 'Books',
        component: () =>
            import(/* webpackChunkName: "group-list" */ '../views/Books.vue'),
    },
    {
        path: '/types',
        name: 'Types',
        props: true,
        component: () =>
            import(/* webpackChunkName: "group-list" */ '../views/Type.vue'),
    },
    {
        path: '/admin',
        name: 'Admin',
        props: true,
        component: () =>
            import(
                /* webpackChunkName: "group-list" */ '../views/Admin/Admin.vue'
            ),
    },
    {
        path: '/admin/books',
        name: 'AdminBooks',
        props: true,
        component: () =>
            import(
                /* webpackChunkName: "group-list" */ '../views/Admin/Books.vue'
            ),
    },
    {
        path: '/admin/types',
        name: 'AdminTypes',
        props: true,
        component: () =>
            import(
                /* webpackChunkName: "group-list" */ '../views/Admin/Types.vue'
            ),
    },
    {
        path: '/books/:id',
        name: 'BookDetails',
        component: () => import('../views/BookDetails.vue'),
    },
];

const router = new VueRouter({
    routes,
});

export default router;
